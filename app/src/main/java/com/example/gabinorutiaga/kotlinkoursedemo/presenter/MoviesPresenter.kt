package com.example.gabinorutiaga.kotlinkoursedemo.presenter

import com.example.gabinorutiaga.kotlinkoursedemo.background.ws.MoviesApi
import com.example.gabinorutiaga.kotlinkoursedemo.model.MoviesDatabase
import com.example.gabinorutiaga.kotlinkoursedemo.model.movie.Movie
import com.example.gabinorutiaga.kotlinkoursedemo.model.MoviesResult
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class MoviesPresenter(private val callback: MoviesCallback) {

    private var call : Call<ArrayList<Movie>> ? = null
    private val db = MoviesDatabase.getInstance()

    fun fetchMovies() {
        // goo.gl/tuezZU
        val localMovies = db?.movieDao()?.movies() as ArrayList<Movie>
        if (localMovies.size == 0) {
            callback.onViewStateChanged(MoviesResult.Loading)
        } else {
            callback.onViewStateChanged(MoviesResult.Success(localMovies))
        }
        call = MoviesApi.api.movies()
        call?.enqueue(object: Callback<ArrayList<Movie>> {
            override fun onFailure(call: Call<ArrayList<Movie>>, t: Throwable) {
                callback.onViewStateChanged(MoviesResult.Error)
            }

            override fun onResponse(call: Call<ArrayList<Movie>>, response: Response<ArrayList<Movie>>) {
                if (response.isSuccessful){
                    val movies = response.body() ?: arrayListOf()

                    movies.size > 0.apply {
                        db?.movieDao()?.deleteAll()
                        movies.forEach {
                            db?.movieDao()?.insertOrUpdate(it)
                        }
                    }
                    callback.onViewStateChanged(MoviesResult.Success(movies))
                }
            }

        })
    }

    private var job: Job? = null

    fun fetchMovies2() {
        job = launch {
            val localMovies = db?.movieDao()?.movies() as ArrayList<Movie>
            withContext(UI) {
                if (localMovies.size == 0) {
                    callback.onViewStateChanged(MoviesResult.Loading)
                } else {
                    callback.onViewStateChanged(MoviesResult.Success(localMovies))
                }
            }
            try {
                val movies = MoviesApi.api.movies2().await()
                movies.size > 0.apply {
                    db.movieDao()?.deleteAll()
                    movies.forEach {
                        db?.movieDao()?.insertOrUpdate(it)
                    }
                }
                withContext(UI) {
                    callback.onViewStateChanged(MoviesResult.Success(movies))
                }
            } catch (ex: Exception) {
                withContext(UI) {
                    callback.onViewStateChanged(MoviesResult.Error)
                    callback.onViewStateChanged(MoviesResult.Success(localMovies))
                }
            }
        }
    }

    fun onDestroy() {
        call?.isExecuted.apply {
            call?.cancel()
        }
        job?.cancel()
    }

}