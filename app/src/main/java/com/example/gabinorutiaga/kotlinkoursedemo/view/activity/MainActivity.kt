package com.example.gabinorutiaga.kotlinkoursedemo.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.example.gabinorutiaga.kotlinkoursedemo.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
